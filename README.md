<center>
<p style="font-size:20pt" align=center> <b> Template collection for quarto project</b> </p>

Quarto is a precompiler for **pandoc** using **revealjs**. 
This makes it easier to create presentations and incorporates plugins and python tools.

</center>


## Getting Started


### Prerequisites

> Which software does the user need to have installed to run your project? 
> Which knowledge should they possess? (Link to potential learning resources) 
> Does the user require any accounts/ access rights with third-party services? 

You need quarto installed [(Link to the installation guide)](https://quarto.org/). 


## Usage

> How to use your project? 
> How to handle everyday use cases? --> this can also be done on a separate documentation page

- Getting started with a project
    
    ```
    $ quarto preview slides.qmd
    ```
    
    This will start a live server in your browser and will automatically update when you change something in `slides.qmd`.


- Add bibliography to your presentation
    
    ```
    ---
    bibliography: /home/tfoerst/literature/literature.bib
    ---
    ```
    
    You should also add a chapter `# Literature` at the end.


## Contributing (optional)

> Put other information here, like where to get help and find more project-related information, 
> e.g., community forums, discord channels, Trello boards 

We are accepting merge requests, bug reports, and feature requests. Feel free to add templates to the `custom/theme` folder.

## License

Distributed under the MIT License. See `LICENSE.md` for more information.

-----

> There are many more sections that can be included here, like 
>
> * Contributors 
> * Contact information 
> * FAQ


